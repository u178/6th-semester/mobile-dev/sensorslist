package com.example.sensorsspinner

import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    val envSensors = listOf<Int>(13, 5, 2, 14, 6, 12)
    val positionSensors = listOf<Int>(1, 35, 11, 15, 20, 9, 4, 16, 36, 30, 8, 17, 29, 19, 18, 10, )
    val humanSensors = listOf<Int>(34, 31, 21)
    val sensorsList = mutableListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sm = getSystemService(SENSOR_SERVICE) as SensorManager
        val textView = findViewById<TextView>(R.id.sensorsTextView)



//        Sensor.TYPE_ACCELEROMETER
//        Log.d("info_sensor", Sensor.STRING_TYPE_ACCELEROMETER)
        val spinner: Spinner = findViewById(R.id.spinner)
        ArrayAdapter.createFromResource(
            this,
            R.array.sensor_types_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                //Log.d("info_sensor", position.toString())
                //Log.d("info_sensor", spinner.adapter.getItem(position).toString())
                sensorsList.clear()

                val sensors = when (spinner.adapter.getItem(position).toString()) {
                    "Датчики окружающей среды" -> envSensors
                    "Датчики положения" -> positionSensors
                    "Датчик состояния человека" -> humanSensors
                    else -> listOf<Int>()
                }

                sm.getSensorList(Sensor.TYPE_ALL).forEach {
                    if (it.type in sensors) {
                        sensorsList.add(it.name)
                    }
                }
                textView.text = sensorsList.joinToString("\n")
            }

        }


    }
}